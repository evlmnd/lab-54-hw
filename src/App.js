import React, {Component} from 'react';
import Card from './components/Card/Card';
import CardDeck from './cardDeck';
import PokerHand from './pokerHand';

import './App.css'


class App extends Component {
    state = {
        cards: new CardDeck().getCards(5)
    };

    shuffleCards = () => {
        const cards = new CardDeck().getCards(5);
        this.setState({cards});
    };


    render() {

        const cards = this.state.cards.map((card) => (
            <Card rank={card.rank}
                  suit={card.suit}
                  key={card.suit + card.rank}
            />
        ));

        const pokerHand = new PokerHand(this.state.cards);

        return (
            <div className="App">
                <button className="shuffle" type="button" onClick={this.shuffleCards}>Shuffle</button>
                <div className="playingCards fourColours faceImages simpleCards inText rotateHand">
                    {cards}
                </div>
                <p className="hand-result">{pokerHand.getOutcome(this.state.cards)}</p>
            </div>
        );
    }
}

export default App;
