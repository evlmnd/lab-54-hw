class CardDeck {
    constructor() {
        this.cards = {
            ranks: [2, 3, 4, 5, 6, 7, 8, 9, 10, 'j', 'q', 'k', 'a'],
            suits: {'diams': '♦', 'hearts': '♥', 'clubs': '♣', 'spades': '♠'} };

        this.cardDeck = [];
        
        this.cards.ranks.forEach((rank) => {
            for (let key in this.cards.suits) {
                let suit = [key, this.cards.suits[key]];
                let newCard = {
                    suit: suit,
                    rank: rank
                };
                this.cardDeck.push(newCard);
            }
        });
    }

    getCard() {
        let randomCardIndex = Math.floor(Math.random() * this.cardDeck.length);
        return this.cardDeck.splice(randomCardIndex, 1)[0];
    }

    getCards(howMany) {
        let cardsArray = [];

        for (let i = 0; i < howMany; i++) {
            let newCard = this.getCard();
            cardsArray.push(newCard);
        }
        return cardsArray;
    }
}

export default CardDeck;