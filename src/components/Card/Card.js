import React from "react"

import './Card.css'

const Card = props => (
    <div className={'Card Card-rank-' + props.rank + ' Card-' + props.suit[0]}>
        <span className="Card-rank">{props.rank}</span>
        <span className="Card-suit">{props.suit[1]}</span>
    </div>
);

export default Card;