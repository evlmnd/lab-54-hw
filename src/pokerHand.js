class PokerHand {
  constructor(cards) {
    this.cards = cards;
    this.hands=["4 of a Kind", "Straight Flush", "Straight", "Flush", "High Card",
      "Pair", "Two Pairs", "Royal Flush", "3 of a Kind", "Full House" ];
  }

  getOutcome() {
    let suitsNumbers = { "spades":1, "clubs":2, "hearts":4, "diams":8 };
    let ranksNumbers = { "a": 14, "k": 13, "q": 12, "j": 11 };

    const suits = this.cards.map(function (card) {
      return suitsNumbers[card.suit[0]];
    });

    const ranks = this.cards.map(function (card) {
      if (card.rank in ranksNumbers) {
        return ranksNumbers[card.rank];
      } else {
        return card.rank;
      }
    });

    return this.rankPokerHand(ranks, suits);
  }

  rankPokerHand(ranks, suits) {
    // Функция определяет все возможные комбинации

    // https://www.codeproject.com/Articles/569271/%2FArticles%2F569271%2FA-Poker-hand-analyzer-in-JavaScript-using-bit-math
    let v, i, o, s = (1<<ranks[0])|(1<<ranks[1])|(1<<ranks[2])|(1<<ranks[3])|(1<<ranks[4]);
    for (i=-1, v=o=0; i<5; i++, o=Math.pow(2,ranks[i]*4)) {v += o*((v/o&15)+1);}
    v = v % 15 - ((s/(s&-s) === 31) || (s === 0x403c) ? 3 : 1);
    v -= (suits[0] === (suits[1]|suits[2]|suits[3]|suits[4])) * ((s === 0x7c00) ? -5 : 1);
    return("Hand: " + this.hands[v] + (s === 0x403c?" (Ace low)":""));
  }

}

export default PokerHand;